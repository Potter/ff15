document.addEventListener('DOMContentLoaded', function () {
    let ff1 = new FF(document.getElementById('game1'));
    let ff2 = new FF(document.getElementById('game2'));
    let ff3 = new FF(document.getElementById('game3'));
});

class FF {
    constructor(container) {
        this.activeItem = null;
        this.emptyItem = null;
        this.targetItem = null;
        this.playground = this.createPlayground();
        this.initEvents(this.playground);
        container.appendChild(this.playground);
    }

    initEvents(playground) {
        playground.addEventListener('dragstart', this.onDragStart.bind(this));
        playground.addEventListener('dragenter', this.onDragEnter.bind(this));
        playground.addEventListener('dragend', this.onDrop.bind(this));
        //TODO add keyboard events
    }

    onDragStart(event) {
        this.setActiveItem(null);
        if (!event.target instanceof HTMLLIElement) {
            return;
        }
        let item = event.target;

        if (item === this.emptyItem) {
            this.setActiveItem(this.emptyItem);
        } else {
            if (this.moveAvailable(item)) {
                this.setActiveItem(item);
            }
        }
    }

    onDragEnter(event) {
        if (!this.activeItem) {
            return;
        }
        if (!event.target instanceof HTMLLIElement) {
            return;
        }

        if ([this.activeItem, this.emptyItem].includes(event.target) || this.activeItem === this.emptyItem && this.moveAvailable(event.target)) {
            this.activeItem.classList.remove('invalid');
            this.targetItem = event.target;
        } else {
            this.activeItem.classList.add('invalid');
            this.targetItem = null;
        }
    }

    onDrop() {
        if (!this.activeItem) {
            return;
        }
        if (!this.targetItem) {
            this.setActiveItem(null);
            return;
        }
        if (this.activeItem !== this.targetItem) {
            let tmpNode = document.createElement('span');
            this.playground.insertBefore(tmpNode, this.activeItem);
            this.playground.insertBefore(this.activeItem, this.targetItem);
            this.playground.insertBefore(this.targetItem, tmpNode);
            this.playground.removeChild(tmpNode);

            this.validatePlayground();
        }
        this.setActiveItem(null);
    }

    moveAvailable(item) {
        let items = [...this.playground.children];
        let itemIndex = items.indexOf(item);
        let emptyIndex = items.indexOf(this.emptyItem);

        let colItem = itemIndex % 4;
        let colEmpty = emptyIndex % 4;

        let rowItem = parseInt(itemIndex / 4);
        let rowEmpty = parseInt(emptyIndex / 4);

        //check if horizontally ok
        if (Math.abs(colEmpty - colItem) === 1 && rowItem === rowEmpty) {
            return true;
        }
        //check if vertical ok
        if (Math.abs(rowEmpty - rowItem) === 1 && colItem === colEmpty) {
            return true;
        }
    }

    setActiveItem(item) {
        if (this.activeItem) {
            this.activeItem.classList.remove('active', 'invalid');
        }
        this.activeItem = item;
        if (this.activeItem) {
            this.activeItem.classList.add('active');
        }
    }

    validatePlayground() {
        let items = [...this.playground.children];
        for (let i = 0; i < 15; i++) {
            if (parseInt(items[i].innerText) - 1 !== i) {
                return false;
            }
        }

        alert('You win!');
    }

    createPlayground(shuffle = true) {
        let playground = document.createElement('ul');
        playground.classList.add('ff-playground');
        let items = [];
        for (let i = 0; i < 16; i++) {
            let item = document.createElement('li');
            item.innerText = i ? i : '';
            item.draggable = true;
            items.push(item);

            if (!i) {
                this.emptyItem = item;
                this.emptyItem.classList.add('empty');
            }
        }

        if (shuffle) {
            items.sort(() => {
                return parseInt(Math.random() * 3) - 1;
            })
        }

        for (let item of items) {
            playground.appendChild(item);
        }
        return playground;
    }
}
